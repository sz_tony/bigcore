package com.ErpCoreModel.Store;

import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.DatabaseType;
import com.ErpCoreModel.Framework.Util;

public class CStore extends CBaseObject{

    private CCustomerMgr customerMgr = null;
    private COrderMgr orderMgr = null;
    private CUnitMgr unitMgr = null;
    private CSpecificationMgr specificationMgr = null;
    private CColorMgr colorMgr = null;
    private CProductMgr productMgr = null;
    private CProductCategoryMgr productCategoryMgr = null;
    private CTypeInCategoryMgr typeInCategoryMgr = null;
    private CProductTypeMgr productTypeMgr = null;
    private CProductInTypeMgr productInTypeMgr = null;
    private CPromotionMgr promotionMgr = null;

    public CPromotionMgr getPromotionMgr()
    {
        if (promotionMgr == null)
        {
            promotionMgr = new CPromotionMgr();
            promotionMgr.Ctx = Ctx;
            if (promotionMgr.getTable().getIsLowChange())
            {
                if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
                    promotionMgr.LoadMongoDb(null, null, false);
                else
                    promotionMgr.Load("", false);
            }
            Ctx.AddBaseObjectMgrCache(promotionMgr.TbCode, Util.GetEmptyUUID(), promotionMgr);
        }
        return promotionMgr;
    }
    public CProductInTypeMgr getProductInTypeMgr()
    {
        if (productInTypeMgr == null)
        {
            productInTypeMgr = new CProductInTypeMgr();
            productInTypeMgr.Ctx = Ctx;
            if (productInTypeMgr.getTable().getIsLowChange())
            {
                if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                productInTypeMgr.LoadMongoDb(null, null, false);
	            else
	                productInTypeMgr.Load("", false);
            }
            Ctx.AddBaseObjectMgrCache(productInTypeMgr.TbCode, Util.GetEmptyUUID(), productInTypeMgr);
        }
        return productInTypeMgr;
    }
    public CProductTypeMgr getProductTypeMgr()
    {
            if (productTypeMgr == null)
            {
                productTypeMgr = new CProductTypeMgr();
                productTypeMgr.Ctx = Ctx;
                if (productTypeMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    productTypeMgr.LoadMongoDb(null, null, false);
	                else
	                    productTypeMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(productTypeMgr.TbCode, Util.GetEmptyUUID(), productTypeMgr);
            }
            return productTypeMgr;
    }
    public CTypeInCategoryMgr getTypeInCategoryMgr()
    {
            if (typeInCategoryMgr == null)
            {
                typeInCategoryMgr = new CTypeInCategoryMgr();
                typeInCategoryMgr.Ctx = Ctx;
                if (typeInCategoryMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    typeInCategoryMgr.LoadMongoDb(null, null, false);
	                else
	                    typeInCategoryMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(typeInCategoryMgr.TbCode, Util.GetEmptyUUID(), typeInCategoryMgr);
            }
            return typeInCategoryMgr;
    }
    public CProductCategoryMgr getProductCategoryMgr()
    {
            if (productCategoryMgr == null)
            {
                productCategoryMgr = new CProductCategoryMgr();
                productCategoryMgr.Ctx = Ctx;
                if (productCategoryMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    productCategoryMgr.LoadMongoDb(null, null, false);
	                else
	                    productCategoryMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(productCategoryMgr.TbCode, Util.GetEmptyUUID(), productCategoryMgr);
            }
            return productCategoryMgr;
    }
    public CProductMgr getProductMgr()
    {
            if (productMgr == null)
            {
                productMgr = new CProductMgr();
                productMgr.Ctx = Ctx;
                if (productMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                {
	                    productMgr.LoadMongoDb(null, null, false);
	                }
	                else
	                    productMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(productMgr.TbCode, Util.GetEmptyUUID(), productMgr);
            }
            return productMgr;
    }
    public CColorMgr getColorMgr()
    {
        
            if (colorMgr == null)
            {
                colorMgr = new CColorMgr();
                colorMgr.Ctx = Ctx;
                if (colorMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    colorMgr.LoadMongoDb(null, null, false);
	                else
	                    colorMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(colorMgr.TbCode, Util.GetEmptyUUID(), colorMgr);
            }
            return colorMgr;
    }
    public CSpecificationMgr getSpecificationMgr()
    {
            if (specificationMgr == null)
            {
                specificationMgr = new CSpecificationMgr();
                specificationMgr.Ctx = Ctx;
                if (specificationMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    specificationMgr.LoadMongoDb(null, null, false);
	                else
	                    specificationMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(specificationMgr.TbCode, Util.GetEmptyUUID(), specificationMgr);
            }
            return specificationMgr;
    }
    public CUnitMgr getUnitMgr()
    {
            if (unitMgr == null)
            {
                unitMgr = new CUnitMgr();
                unitMgr.Ctx = Ctx;
                if (unitMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                    unitMgr.LoadMongoDb(null, null, false);
	                else
	                    unitMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(unitMgr.TbCode, Util.GetEmptyUUID(), unitMgr);
            }
            return unitMgr;
    }
    public COrderMgr getOrderMgr()
    {
            if (orderMgr == null)
            {
                orderMgr = new COrderMgr();
                orderMgr.Ctx = Ctx;
                if (orderMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                {
	                    orderMgr.LoadMongoDb(null, null, false);
	                }
	                else
	                    orderMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(orderMgr.TbCode, Util.GetEmptyUUID(), orderMgr);
            }
            return orderMgr;
    }
    public CCustomerMgr getCustomerMgr()
    {
            if (customerMgr == null)
            {
                customerMgr = new CCustomerMgr();
                customerMgr.Ctx = Ctx;
                if (customerMgr.getTable().getIsLowChange())
                {
                    if (Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
	                {
	                    customerMgr.LoadMongoDb(null, null, false);
	                }
	                else
	                    customerMgr.Load("", false);
                }
                Ctx.AddBaseObjectMgrCache(customerMgr.TbCode,Util.GetEmptyUUID(), customerMgr);
            }
            return customerMgr;
    }
}

