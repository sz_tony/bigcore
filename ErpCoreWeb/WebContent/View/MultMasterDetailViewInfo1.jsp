<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewCatalog" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.enumViewType" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.util.HashMap" %>
    
<%
//暂时不支持 多主从表视图
response.getWriter().print("不支持 多主从表视图");
response.getWriter().close();

if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CView m_View=null;
String id = request.getParameter("id");
boolean m_bIsNew=false;
if (!Global.IsNullParameter(id))
{
    m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
}
else
{
    m_bIsNew = true;
    if (request.getSession().getAttribute("NewMultMasterDetailView") == null)
    {
        m_View = new CView();
        m_View.Ctx = Global.GetCtx(this.getServletContext());
        m_View.setVType( enumViewType.MultMasterDetail);

        Map<UUID, CView> sortObj = new HashMap<UUID, CView>();
        sortObj.put(m_View.getId(), m_View);
        request.getSession().setAttribute("NewMultMasterDetailView", sortObj);
    }
    else
    {
    	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewMultMasterDetailView");
        m_View =(CView) sortObj.values().toArray()[0];
    }
}
%>
<%!
String LoadCatalog()
{
    String sJson="";
    List<CBaseObject> lstObj = Global.GetCtx(this.getServletContext()).getViewCatalogMgr().GetList();
    for (CBaseObject obj : lstObj)
    {
        CViewCatalog catalog = (CViewCatalog)obj;
        sJson += String.format("{ text: '%s', id: '%s' },",catalog.getName(), catalog.getId().toString());
    }
	if (sJson.endsWith(","))
		sJson = sJson.substring(0, sJson.length() - 1);
	sJson = "[" + sJson + "]";
	
	return sJson;
}
String LoadTable()
{
	String sJson="";
    List<CBaseObject> lstObj = Global.GetCtx(this.getServletContext()).getTableMgr().GetList();
    for (CBaseObject obj : lstObj)
    {
        CTable table = (CTable)obj;
        sJson += String.format("{ text: '%s', id: '%s' },",table.getName(), table.getId().toString());
    }
	if (sJson.endsWith(","))
		sJson = sJson.substring(0, sJson.length() - 1);
	sJson = "[" + sJson + "]";
	
	return sJson;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    
    <script type="text/javascript">

    function btUp_Click() {
        var row = grid.getSelectedRow();
        if (row == null) {
            $.ligerDialog.alert('请选择行!');
            return;
        }
        grid.up( grid.getSelectedRow());
    } 
    function btDown_Click() {
        var row = grid.getSelectedRow();
        if (row == null) {
            $.ligerDialog.alert('请选择行!');
            return;
        }
        grid.down(grid.getSelectedRow());
    } 
    function btDel_Click() {
        var row = grid.getSelectedRow();
        if (row == null) {
            $.ligerDialog.alert('请选择行!');
            return;
        }
        grid.deleteSelectedRow();
    } 
    function btAdd_Click() {
        $.post(
            'MultMasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                Action: 'Add'
            },
             function (data) {
                 if (data == "" || data == null) {
                     return true;
                 }
                 else {
                     $.ligerDialog.warn(data);
                     return false;
                 }
             },
             'text');
    }   
    function onNext() {
        $.post(
            'MultMasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                Action: 'Next'
            },
             function (data) {
                 if (data == "" || data == null) {
                     return true;
                 }
                 else {
                     $.ligerDialog.warn(data);
                     return false;
                 }
             },
             'text');
    }    
    function onCancel() {
        $.post(
            'MultMasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                Action: 'Cancel'
            },
             function (data) {
                 if (data == "" || data == null) {
                	 parent.$.ligerDialog.close();
                     return true;
                 }
                 else {
                     $.ligerDialog.warn(data);
                     return false;
                 }
             },
             'text');
    }   
    
    var grid;
    $(function ()
    {
        grid = $("#listViewDetail").ligerGrid({
        columns: [
            { display: '名称', name: 'Name',align: 'left'}
            ],
            url: 'MultMasterDetailViewInfo1.do?Action=GetViewDetail&id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %>',
            dataAction: 'server', 
            usePager: false,
            width: '100%', height: '100%',
            onSelectRow: function (data, rowindex, rowobj)
            {
                //$.ligerDialog.alert('1选择的是' + data.id);
            },
            onUnSelectRow: function (data, rowindex, rowobj)
            {
                //alert('反选择的是' + data.id);
            }
        });
    });


    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" >
    <div >
        <p>基本定义：</p>
        <br />
        
        <table cellpadding="0" cellspacing="0" class="l-table-edit" style="height:50px">
            <tr>
                <td align="left">
                    &nbsp;名称：</td>
                <td align="left">
                    &nbsp;<input id="txtName" name="txtName" />
                    </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;目录</td>
                <td align="left">
                    &nbsp;
                    <input id="cbCatalog" name="cbCatalog" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;主表：</td>
                <td align="left">
                    &nbsp;
                    <input id="cbMasterTable" name="cbMasterTable" />
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    &nbsp;从表：</td>
                <td align="left">
                    <table><tr><td>
   					<div id="listViewDetail" style="margin:0; padding:0;width:174px;height:88px"></div>
                    </td><td valign="bottom">
            <input type="button" id="btUp" style="width:60px" value="向上" onclick="btUp_Click();" />
                        <br />
            <input type="button" id="btDown" style="width:60px" value="向下" onclick="btDown_Click();" />
                        <br />
            <input type="button" id="btDel" style="width:60px" value="删除" onclick="btDel_Click();" />
                    </td></tr></table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;外键关联：</td>
                <td align="left">
                    &nbsp;
                    <input id="cbPrimaryKey" name="cbPrimaryKey" />
                    ——
                    <input id="cbForeignKey" name="cbForeignKey" />
                </td>
            </tr>
            <tr>
            <td></td>
            <td>
                <input id="cbDetailTable" name="cbDetailTable" />
            	<input type="button" id="btAdd" style="width:60px" value="添加从表" onclick="btAdd_Click();" />
            </td></tr>
        </table>
        <br />
        <p>
            <input type="button" id="btNext" style="width:60px" value="下一步" onclick="onNext();" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btCancel" style="width:60px" value="取消" onclick="onCancel();" />
            </p>
    </div>
    </form>
</body>
</html>