<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.UI.CColumnInViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
String vid = request.getParameter("vid");
if (Global.IsNullParameter(vid))
{
    response.getWriter().close();
}
String vdid = request.getParameter("vdid");
if (Global.IsNullParameter(vdid))
{
	response.getWriter().close();
}
CView m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(vid));
if (m_View==null)
{
	response.getWriter().close();
}
CViewDetail m_ViewDetail = (CViewDetail)m_View.getViewDetailMgr().Find(Util.GetUUID(vdid));
if (m_ViewDetail == null)
{
	response.getWriter().close();
}
CTable m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_ViewDetail.getFW_Table_id());

%>
<%!
public List<CColumn> GetDetailColList(CViewDetail ViewDetail,CTable Table)
{
    List<CColumn> lstRet = new ArrayList<CColumn>();

    List<CBaseObject> lstObjCIVD = ViewDetail.getColumnInViewDetailMgr().GetList();
    for (int i = 0; i < lstObjCIVD.size(); i++)
    {
        CColumnInViewDetail civd = (CColumnInViewDetail)lstObjCIVD.get(i);
        CColumn col = (CColumn)Table.getColumnMgr().Find(civd.getFW_Column_id());
        if (col == null)
            continue;
        lstRet.add(col);
    }
    return lstRet;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <script type="text/javascript">
        
    </script>
    <style type="text/css">
    #menu1,.l-menu-shadow{top:30px; left:50px;}
    #menu1{  width:200px;}
    </style>
    
    <script type="text/javascript">
                
        var grid;
        $(function ()
        {
            grid = $("#gridDetail").ligerGrid({
            columns: [
                <%
                List<CColumn> lstDetailCol=GetDetailColList(m_ViewDetail,m_Table);
                for (int i=0;i<lstDetailCol.size();i++)
                {
                    CColumn col = lstDetailCol.get(i);
                    if(i<lstDetailCol.size()-1)
                    	out.print(String.format("{ display: '%s', name: '%s',align: 'left'},",col.getName(),col.getCode()));
                    else
                    	out.print(String.format("{ display: '%s', name: '%s',align: 'left'}",col.getName(),col.getCode()));
                }
                 %>
                ],
                url: 'MultDetailList.do?Action=GetData&vid=<%=request.getParameter("vid") %>&vdid=<%=request.getParameter("vdid") %>&pid=<%=request.getParameter("pid") %>&ParentId=<%=request.getParameter("ParentId") %>',
                dataAction: 'server', 
                usePager: false,
                width: '100%', height: '100%',
                onSelectRow: function (data, rowindex, rowobj)
                {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                },
                onUnSelectRow: function (data, rowindex, rowobj)
                {
                    //alert('反选择的是' + data.id);
                }
            });
        });

    </script>
</head>
<body style="padding:6px; overflow:hidden;"> 
   <div id="gridTable" style="margin:0; padding:0"></div>
   <div id="gridDetail" style="margin:0; padding:0"></div>

</body>
</html>