

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CContext;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.request=request;
		this.response=response;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String sUserName=request.getParameter("txtName");
		String sPwd=request.getParameter("txtPwd");
		
		CContext ctx = Global.GetCtx(this.getServletContext());
        if (ctx == null)
        {
        	response.getWriter().println("<script>alert('单位没有注册！');</script>");
            return;
        }
        CUser user = ctx.getUserMgr().FindByName(sUserName);
        if (user == null)
        {
        	response.getWriter().println("<script>alert('用户不存在！');</script>");
            return;
        }
        if (!user.getPwd().equals(sPwd))
        {
        	response.getWriter().println("<script>alert('密码不正确！');</script>");
            return;
        }

        //Session["TopCompany"] = txtCompany.Text.Trim();
        request.getSession().setAttribute("TopCompany", ctx.getCompanyMgr().FindTopCompany().getName());
        request.getSession().setAttribute("User", user);

        response.getWriter().println("<script>parent.window.location.reload();</script>");
	}

}
