package com.ErpCoreWeb.Report;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Report.CReportCatalog;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class AddCatalog
 */
@WebServlet("/Report_AddCatalog")
public class AddCatalog extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CTable m_Table = null;
    public CCompany m_Company = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCatalog() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
	void initData() {
		if (request.getSession().getAttribute("User") == null) {
			try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		String B_Company_id = request.getParameter("B_Company_id");
		if (Global.IsNullParameter(B_Company_id))
			m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr()
					.FindTopCompany();
		else
			m_Company = (CCompany) Global.GetCtx(this.getServletContext())
					.getCompanyMgr().Find(Util.GetUUID(B_Company_id));

		m_Table = m_Company.getReportCatalogMgr().getTable();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("PostData"))
        {
        	PostData();
            return ;
        }
	}

    void PostData()
    {
		try {
			CUser user = (CUser) request.getSession().getAttribute("User");

			CReportCatalog BaseObject = new CReportCatalog();
			BaseObject.Ctx = Global.GetCtx(this.getServletContext());
			BaseObject.setCreator(user.getId());
			List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
			boolean bHasVisible = false;
			for (CBaseObject obj : lstCol) {
				CColumn col = (CColumn) obj;

				if (col.getCode().equalsIgnoreCase("id"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Created"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Creator"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Updated"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Updator"))
					continue;

				BaseObject
						.SetColValue(col, request.getParameter(col.getCode()));
				bHasVisible = true;
			}
			if (!bHasVisible) {
				response.getWriter().print("没有可修改字段！");
				return;
			}
			if (!m_Company.getReportCatalogMgr().AddNew(BaseObject, true)) {
				response.getWriter().print("添加失败！");
				return;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
